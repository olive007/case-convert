from case_convert import *


if __name__ == '__main__':
    print(camel_case("helloGreat world"))
    print(kebab_case("__hello Great--world"))
    print(pascal_case("__hello Great--world"))
    print(snake_case("__hello Great--world"))
    print(upper_case("__hello Great--world"))

    print(camel_case("locationApiary"))
    print(kebab_case("locationApiary"))
    print(pascal_case("locationApiary"))
    print(snake_case("locationApiary"))
    print(upper_case("locationApiary"))

import { camelCase, kebabCase, pascalCase, snakeCase, upperCase } from "case-convert";

console.log(camelCase("helloGreat world"));
console.log(kebabCase("__hello Great--world"));
console.log(pascalCase("__hello Great--world"));
console.log(snakeCase("__hello Great--world"));
console.log(upperCase("__hello Great--world"));

console.log(camelCase("locationApiary"));
console.log(kebabCase("locationApiary"));
console.log(pascalCase("locationApiary"));
console.log(snakeCase("locationApiary"));
console.log(upperCase("locationApiary"));
